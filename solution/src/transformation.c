#include "../include/transformation.h"

#include "stdlib.h"

struct image* rotate_90(struct image * const source) {
    ROTATION(source->height, source->width, source->data[source->width * (j + 1) - 1 - i])
}
struct image* rotate_minus_90(struct image* source) {
    ROTATION(source->height, source->width, source->data[source->width * (source->height - 1 - j) + i])
}

struct image* rotate_180(struct image* source) {
    ROTATION(source->width, source->height, source->data[source->width * (source->height - 1 - i) + (source->width - 1 - j)])
}

struct image* rotate_image(int16_t angle, struct image* img) {
    struct image* rotated = 0;
    if (angle == 0 || abs(angle) == 180) {
        rotated = (angle == 0) ? copy_image(img) : rotate_180(img);
    } else {
        if (abs(angle) == 90) {
            rotated = (angle < 0) ? rotate_minus_90(img) : rotate_90(img);
        } if (abs(angle) == 270){
            rotated = (angle < 0) ? rotate_90(img) : rotate_minus_90(img);
        }
    }
    return rotated;
}

