#include "../include/bmp.h"
#include "../include/file_handler.h"
#include "../include/image_processor.h"
#include "../include/io.h"
#include "../include/transformation.h"
#include <stdlib.h>

int main(int argc, char** argv ) {

    if (validate_input(argc) != ARGS_VALID) {
        return 1;
    }

    char* input_image = argv[1];
    char* output_image = argv[2];

    int16_t angle = 0;
    angle_status read_angle_status = read_angle(argv[3], &angle);

    if (read_angle_status != ANGLE_VALID) {
        print_error(angle_status_names[read_angle_status]);
        print_error("Acceptable angle values: 0, 90, -90, 180, -180, 270, -270");
        return 1;
    }

    FILE *src_file;
    file_status open_file_status = read_file(input_image, &src_file);

    if (open_file_status != FILE_OK) {
        print_error(file_proc_status_names[open_file_status]);
        return 1;
    }

    struct image* img;
    read_code_t read_bmp_status = from_bmp(src_file, &img);

    if (read_bmp_status != READ_OK) {
        destroy_image(img);
        print_error(read_status_names[read_bmp_status]);
        return 1;
    }
    fclose(src_file);

    struct image* rotated;
    rotated = rotate_image(angle, img);

    FILE* out_file;
    open_file_status = write_file(output_image, &out_file);

    if (open_file_status != FILE_OK) {
        destroy_image(rotated);
        destroy_image(img);
        print_error(file_proc_status_names[open_file_status]);
        return 1;
    }

    write_code_t write_bmp_status = to_bmp(out_file, rotated);

    if (write_bmp_status != WRITE_OK) {
        destroy_image(rotated);
        destroy_image(img);
        print_error(write_status_names[write_bmp_status]);
        return 1;
    }
    fclose(out_file);
    destroy_image(img);
    destroy_image(rotated);

    return 0;
}
