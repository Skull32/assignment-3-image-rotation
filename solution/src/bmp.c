#include "../include/bmp.h"

#define BYTE_ALIGNMENT 4
#define BMP_TYPE 0x4D42
#define BI_RESERVED 0
#define BI_SIZE 40
#define BI_PLANES 1
#define BI_BIT_COUNT 24
#define BI_X_PELS_PER_METER 2835
#define BI_Y_PELS_PER_METER 2835
#define BI_COMPRESSION 0
#define BI_CLR_USED 0
#define BI_CLR_IMPORTANT 0


uint32_t calc_padding(uint32_t width) {
    return (BYTE_ALIGNMENT - ((width * sizeof (struct pixel)) % BYTE_ALIGNMENT) % BYTE_ALIGNMENT);
}

struct bmp_header create_bmp_header(struct image const* img) {
    struct bmp_header header;
    uint32_t row_padding = calc_padding(img->width);
    uint32_t row_size = img->width * STRUCT_PIXEL_SIZE;
    uint32_t size_image = (row_size  + row_padding) * img->height;

    header.bfType = BMP_TYPE;
    header.bfileSize = size_image + STRUCT_BMP_HEADER_SIZE;
    header.bfReserved = BI_RESERVED;
    header.bOffBits = STRUCT_BMP_HEADER_SIZE;
    header.biSize = BI_SIZE;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = BI_PLANES;
    header.biBitCount = BI_BIT_COUNT;
    header.biCompression = BI_COMPRESSION;
    header.biSizeImage = size_image;
    header.biXPelsPerMeter = BI_X_PELS_PER_METER;
    header.biYPelsPerMeter = BI_Y_PELS_PER_METER;
    header.biClrUsed = BI_CLR_USED;
    header.biClrImportant = BI_CLR_IMPORTANT;

    return header;
}


