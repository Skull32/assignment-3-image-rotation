#include "../include//image.h"
#include <stdlib.h>

struct image *create_image(uint64_t width, uint64_t height) {
    struct image *img = malloc(sizeof(struct image));
    if (img == NULL) {
        free(img);
        return NULL;
    }
    img->width = (uint32_t) width;
    img->height = (uint32_t) height;
    img->data = malloc(STRUCT_PIXEL_SIZE * width * height);
    if (img->data == NULL) {
        free(img->data);
        free(img);
        return NULL;
    }
    return img;
}

void destroy_image(struct image* image) {
    if (image->data != NULL) {
        free(image->data);
    }
    free(image);
}

void destroy_image_memory(struct image* image) {
    if (image->data != NULL) {
        free(image->data);
        image->data = NULL;
    }
}

struct image* copy_image(struct image* source) {
    struct image* result = create_image(source->width, source->height);
    if (result == NULL) {
        free(result);
        return NULL;
    }
    for (size_t i = 0; i < result->height; i++) {
        for (size_t j = 0; j < result->width; j++) {
            result->data[i * result->width + j]
                    = source->data[i * result->width + j];
        }
    }
    return result;
}
