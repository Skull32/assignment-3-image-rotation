#include "../include/utils.h"
#include "stdio.h"

void print_error(char *string) {
    fprintf(stderr, "\n%s\n", string);
}
