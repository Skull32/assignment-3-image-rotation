#ifndef FILE_HANDLER_H
#define FILE_HANDLER_H

#include <stdio.h>

extern char* file_proc_status_names[2];

typedef enum file_status {
    FILE_OK,
    FILE_ERROR
} file_status;

file_status read_file(char* filename, FILE** f);
file_status write_file(char* filename, FILE** f);

#endif //FILE_HANDLER_H
