#ifndef TRANSFORMATION_H
#define TRANSFORMATION_H


#include "image.h"
#include <inttypes.h>
#include <stdbool.h>

#define ROTATION(x, y, z)    \
    struct image* result = create_image(x, y); \
    if (result == NULL) {    \
        free(result);\
        return NULL;\
    }\
    for (size_t i = 0; i < result -> height; i++) {                           \
        for (size_t j = 0; j < result -> width; j++) {                        \
            result->data[i * result -> width + j] = (z);     \
        }                                                                           \
    }    \
    return result;

struct image* rotate_image(int16_t angle, struct image* img);

#endif //TRANSFORMATION_H
